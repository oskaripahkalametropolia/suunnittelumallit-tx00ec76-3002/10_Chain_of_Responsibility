package boss;

public interface RaiseHandler {
    void setSuperior(RaiseHandler superior);
    boolean handleRaiseRequest(double percentage);
    RaiseHandler getSuperior();
}
