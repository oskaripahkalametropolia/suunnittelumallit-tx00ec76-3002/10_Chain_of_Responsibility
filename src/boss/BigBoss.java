package boss;

public abstract class BigBoss implements RaiseHandler{
    private RaiseHandler superior;

    @Override
    public void setSuperior(RaiseHandler superior) {
        this.superior = superior;
    }

    @Override
    public boolean handleRaiseRequest(double percentage) {
        if (superior != null){
            return superior.handleRaiseRequest(percentage);
        }
        return false;
    }

    @Override
    public RaiseHandler getSuperior(){
        return this.superior;
    }
}
