import boss.BigBoss;

public class Päällikkö  extends BigBoss {
    @Override
    public boolean handleRaiseRequest(double percentage){
        if (percentage > 0.02 && percentage < 0.05){
            System.out.println("Yksikön päällikkö hyväksyi palkankorotuksen.");
            return true;
        }
        if (getSuperior() != null){
            return super.handleRaiseRequest(percentage);
        }
        System.out.println("Yksikön päällikkö ei hyväksynyt palkankorotusta.");
        return false;
    }
}
