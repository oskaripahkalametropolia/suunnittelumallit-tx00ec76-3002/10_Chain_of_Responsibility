import boss.BigBoss;

public class Main {
    public static void main(String[] args) {
        BigBoss esimies = new Lähiesimies();
        BigBoss päällikkö = new Päällikkö();
        BigBoss johtaja = new Toimitusjohtaja();

        esimies.setSuperior(päällikkö);
        päällikkö.setSuperior(johtaja);

        esimies.handleRaiseRequest(0.02);
        esimies.handleRaiseRequest(0.035);
        esimies.handleRaiseRequest(1.00);
    }
}
