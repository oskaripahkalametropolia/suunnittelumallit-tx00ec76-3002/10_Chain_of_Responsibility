import boss.BigBoss;

import java.util.Random;

public class Toimitusjohtaja extends BigBoss {
    @Override
    public boolean handleRaiseRequest(double percentage){
        if (percentage >= 0.05 && new Random().nextBoolean()){
            System.out.println("Toimitusjohtaja hyväksyi palkankorotuksen.");
            return true;
        }
        if (getSuperior() != null){
            return super.handleRaiseRequest(percentage);
        }
        System.out.println("Toimitusjohtaja ei hyväksynyt palkankorotusta.");
        return false;
    }
}
