import boss.BigBoss;

public class Lähiesimies  extends BigBoss {
    @Override
    public boolean handleRaiseRequest(double percentage){
        if (percentage <= 0.02){
            System.out.println("Lähiesimies hyväksyi palkankorotuksen.");
            return true;
        }
        if (getSuperior() != null){
            return super.handleRaiseRequest(percentage);
        }
        System.out.println("Lähiesimies ei hyväksynyt palkankorotusta.");
        return false;
    }
}
